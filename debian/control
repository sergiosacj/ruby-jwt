Source: ruby-jwt
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Markus Tornow <tornow@riseup.net>,
           Pirate Praveen <praveen@debian.org>
Section: ruby
Testsuite: autopkgtest-pkg-ruby
Priority: optional
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               rake,
               ruby-rspec,
               ruby-rbnacl
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-jwt.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-jwt
Homepage: https://github.com/jwt/ruby-jwt
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-jwt
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: ruby-oauth2 (<< 1.4.1~),
        ruby-omniauth-azure-oauth2 (<< 0.0.10~),
        ruby-googleauth (<< 0.6~)
Description: JSON Web Token implementation in Ruby
 A JSON Web Token (JWT) is a compact token format intended for space
 constrained environments such as HTTP Authorization headers and URI query
 parameters, and used as a means of representing claims to be transferred
 between two parties. The claims in a JWT are encoded as a JSON object that is
 digitally signed.
 .
 The JWT specification supports several algorithms for cryptographic signing.
 This library currently supports HMAC (HS256, HS384, HS512) and RSA
 (RS256, RS256, RS512). Unsigned plaintext JWT's are supported too.
